import React, { useState, useEffect } from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,MDBBtn,MDBIcon } from 'mdbreact';
import User from './User';

export default function ListeUsers() {
    const [users, setUsers] = useState([]);
    const [suppr, setSuppr] = useState([]);

    const getUsers =  () => {
        fetch(`${process.env.REACT_APP_API_URL}/personne`)
            .then(response => {return response.json();})
            .then(data=>setUsers(data))
            .catch(error => {console.log(error);}); 
    }

    useEffect(() => {
        getUsers();
    }, [suppr]);

   function suppression(){
        setSuppr(!suppr);
    }
    
    const usersList = users.map((user) => <User key={user.id} user1={user} suppression={suppression}/>);

    return <div class="container listeUser">
        <div>
        <MDBBtn floating size="sm">
        <MDBIcon  MDBIcon icon="plus-circle"  />
      </MDBBtn>
        </div>
        <h3>Liste des Utilisateurs :</h3> 
        < MDBTable striped hover>
        <MDBTableHead><tr>
                <th>Login</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Mail</th>
                <th>telephone</th>
                <th>Fonction</th>
                <th >Role</th>
                <th></th>
                <th></th></tr></MDBTableHead>
                <MDBTableBody>{usersList}</MDBTableBody></MDBTable>
        </div>;
}