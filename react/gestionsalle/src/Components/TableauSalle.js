import React, { useState, useEffect } from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,MDBBtn,MDBIcon, MDBRow,MDBCardGroup } from 'mdbreact';
import Salle from './Salle';

export default function TAbleauSalle() {
    const [salles, setSalles] = useState([]);
    const [suppr, setSuppr] = useState([]);

    const getSalles =  () => {
        fetch(`${process.env.REACT_APP_API_URL}/salle`)
            .then(response => {return response.json();})
            .then(data=>setSalles(data))
            .catch(error => {console.log(error);}); 
    }

    useEffect(() => {
        getSalles();
    }, [suppr]);

   function suppression(){
        setSuppr(!suppr);
    }
    
    const salleListe = salles.map((salle) => <Salle key={salle.id} salle={salle}/>);

    return <div class="listeSalle">
        <h3>Liste des salles :</h3> 
        <MDBCardGroup>
        {salleListe}</MDBCardGroup>
        </div>;
}