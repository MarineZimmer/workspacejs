import React, { useState, useEffect, useRef } from 'react';
import $ from 'jquery'
import { MDBContainer, MDBCol, MDBBtn,MDBInput,MDBRow } from "mdbreact";
import '../css/Authentification.css'


export default function Login(props) {

  const [authentification,setAuthentification]=useState({});
  const [erreur,setErreur]=useState("");

     async function submitForm(event){
        event.preventDefault();
        let response =await  fetch(`${process.env.REACT_APP_API_URL}/auth`, {
          method: "POST",
          body: JSON.stringify(authentification),
          headers: {
            'Accept': 'application/json',
            "Content-type": "application/json; charset=UTF-8"
          }
        })
        .then(response=>{return response.json()})
        .catch(error => console.log(error));
         
       
        if(response){
          console.log("oui")
          document.location.href ="/admin/user";
        }else{
          console.log("non")
          setErreur("Erreur login ou mot de passe incorrect")   }
    }
    
    function changeHandler(event) {
      let authentificationTemp = authentification
      authentificationTemp[event.target.name]=event.target.value;
      setAuthentification(authentification)
     };

return(
  <div className="auth">
    
    <div className="authHeader">
    <img  className="imgHeader"src="/img/logo.png" alt="logo" />
    Partie administrateur
    </div>
<MDBContainer >
<MDBRow>
  <MDBCol md="5">
  <div className="container loginBox">
    <form onSubmit={submitForm}>
      <p className="h3 text-center mb-4" >Connexion</p>
      <p className="h4 text-center mb-4 red-text" >{erreur}</p>
      <div className="white-text">
      <MDBInput id="login" label="Login" name="login" icon="user" group type="text" prepend="Large"
          size="lg" onChange={changeHandler} validate error="wrong"
            success="right" className="white-text form-control form-control-lg" />
        <MDBInput label="mot de passe" name="mdp" icon="lock" group type="password"  prepend="Large"
          size="lg" onChange={changeHandler} className="white-text form-control form-control-lg" validate />
      </div>
      <div className="text-center">
        <MDBBtn type="submit">Connexion</MDBBtn>
      </div>
    </form>
    <br/>
    <div className="text-right ">
<a className="lienAuth" href="/">Connexion gestion salle</a>
</div>
    </div>
  </MDBCol>
</MDBRow>
</MDBContainer>
</div>);

}