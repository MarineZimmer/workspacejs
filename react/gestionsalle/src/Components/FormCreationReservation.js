import React, { useState, useEffect, useRef } from 'react';
import $ from 'jquery'
import { MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";

export default function FormCreationUser({rechercheSalle}) {


  const[reservation,setReservation] = useState({
    "id":"0",
    "dateDebut":"",
    "dateFin":"",
    "nomReservation":""
  })

  

  function changeHandler(event) {
    let reservationTemp = reservation
    
    reservationTemp[event.target.name] = event.target.value;
    
    setReservation(reservationTemp)
  };



  async function submitForm(event) {
    event.preventDefault();
   rechercheSalle(reservation);

  };

  useEffect(() => {
   
   
  }, []);

  return <div className="formReservation">
    
    <h4>Informations Reservation : </h4>
    <form
      className="needs-validation"
      onSubmit={submitForm}
      noValidate
    >
     
      <MDBRow>
          <MDBInput
            valueDefault={reservation.nomReservation}
            name="nomReservation"
            onChange={changeHandler}
            type="text"
            id="nom"
            label="Nom"
            required
          >
            <div className="valid-feedback">Looks good!</div>
          </MDBInput>
          </MDBRow>
          <MDBRow>
        <MDBInput
            valueDefault={reservation.dateDebut}
            name="dateDebut"
            onChange={changeHandler}
            type="date"
            id="dateDebut"
            label="Date de debut"
            required>
            <div className="invalid-feedback">Looks good!</div>
            <div className="valid-feedback">Looks good!</div>
          </MDBInput></MDBRow>
          <MDBRow>
        <MDBInput
            valueDefault={reservation.dateFin}
            name="dateFin"
            onChange={changeHandler}
            type="date"
            id="dateFin"
            label="Date de fin"
            required>
            <div className="invalid-feedback">Looks good!</div>
            <div className="valid-feedback">Looks good!</div>
          </MDBInput></MDBRow>
         <MDBRow><MDBBtn type="submit">Afficher les salles dsponibles</MDBBtn></MDBRow>
    </form> 
  </div>
}