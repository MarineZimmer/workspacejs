import React, { useState, useEffect, useRef } from 'react';
import { MDBIcon } from "mdbreact";
import $ from 'jquery';

export default function User({ rowObjet, setRefresh,type}) {

    const [objet, setObjet] = useState(rowObjet);
    const refInputModif = useRef(null);


    async function modifier() {
        refInputModif.current.setAttribute("disabled","true");
        objet.libelle=refInputModif.current.value;
        let response = await fetch(`${process.env.REACT_APP_API_URL}`+"/"+type, {
            method: "PUT",
            body: JSON.stringify(objet),
            headers: {
                //"Content-Type": "multipart/form-data" }
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8"
            }
        })
    }

    function editable() {
        refInputModif.current.removeAttribute("disabled");
        refInputModif.current.focus();
    }

    useEffect(() => {
       // const tabTodo = props.todos;
       // tabTodo[props.pos] = todo;
       // props.setTodos(tabTodo);

    }, [])

    async function supprimer() {
       
     if (window.confirm("voullez vous supprimer le " + type + " " + objet.libelle)) {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/`+type+"/" + objet.id, {
            method: "DELETE"
        }).then(response => { return response.json(); })
            .catch(error => { console.log(error); });
        alert("post supprime ");
        setRefresh();
    }

    }

    

    return <tr>
        <td><input type="text" id={objet.id} className="inputGestion" defaultValue={objet.libelle} ref={refInputModif} disabled onBlur={modifier} /></td>
        <td onClick={editable}><MDBIcon icon="pencil-alt" /></td>
        <td onClick={supprimer}><MDBIcon far icon="trash-alt" /></td>
        </tr>
}