import React, { useState, useEffect } from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,MDBBtn,MDBIcon, MDBRow,MDBCollapse } from 'mdbreact';
import Salle from './Salle';

export default function TAbleauSalle() {
    const [typeSalles, setTypesSalles] = useState([]);
    const [typeMateriels, setTypesMateriels] = useState([]);
    const [collapseID, setCollapseID] = useState(false);
    
      
     function toggleCollapse(test) {
      setCollapseID(!collapseID)
    }

    const getTypeSalles =  () => {
        fetch(`${process.env.REACT_APP_API_URL}/typesalle`)
            .then(response => {return response.json();})
            .then(data=>setTypesSalles(data))
            .catch(error => {console.log(error);}); 
    }
    const getTypeMateriels =  () => {
        fetch(`${process.env.REACT_APP_API_URL}/typemateriel`)
            .then(response => {return response.json();})
            .then(data=>setTypesMateriels(data))
            .catch(error => {console.log(error);}); 
    }

    useEffect(() => {
        getTypeSalles();
        getTypeMateriels();
    }, []);

   

    return <div className="filtre">
  
      <MDBBtn
      onClick={toggleCollapse}
    >Filtrer les salles
    </MDBBtn>
    
        <MDBCollapse id="basicCollapse" isOpen={collapseID}>
          <p>
            tout les filtre
          </p>
        </MDBCollapse>
        </div>;
}