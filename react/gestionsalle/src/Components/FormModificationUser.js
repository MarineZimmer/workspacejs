import React, { useState, useEffect, useRef, Fragment } from 'react';
import $ from 'jquery'
import { MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";
import {useParams} from 'react-router-dom'

export default function FormModificationUser(props) {
  const [user, setUser] = useState({});
  const [fonctions, setFonctions] = useState([]);
  const [roles, setRoles] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const { login } = useParams();


  const getFonctions =  () => {
     fetch(`${process.env.REACT_APP_API_URL}/fonction`)
      .then(response => response.json())
      .then(data => setFonctions(data))
      .catch(error => console.log(error))
  }

  const getRoles =  () => {
     fetch(`${process.env.REACT_APP_API_URL}/role`)
      .then(response => response.json())
      .then(data => setRoles(data))
      .catch(error => console.log(error))
  }

  const getUser = async () => {
    await fetch(`${process.env.REACT_APP_API_URL}/personne/${login}`)
      .then(response => response.json())
      .then(data => setUser(data))
      .catch(error => console.log(error))
  }

  function changeHandler(event) {
    let userTemp = user
    if (event.target.name === "fonction") {
      userTemp.fonction = fonctions.find(e => e.id == event.target.value)
    } else if (event.target.name === "role") {
      userTemp.role = roles.find(e => e.id == event.target.value)
    } else if (event.target.name === "mdp") {
      userTemp.authentification.mdp = event.target.value
    } else if (event.target.name === "login") {
      userTemp.authentification.login = event.target.value
    } else {
      userTemp[event.target.name] = event.target.value;
    }
    setUser(userTemp)
  };



  async function submitForm(event) {
    event.preventDefault();
    let responseData = await fetch(`${process.env.REACT_APP_API_URL}/personne/update`, {
      method: "PUT",
      body: JSON.stringify(user),
      headers: {
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8"
      }
    })
    .then(response=>response.json())
    .catch(error => console.log(error));
    if (responseData) {
      alert("ok")
    } else {
      alert("ko")
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      await getUser();
      setIsLoading(false);
    }
    fetchData()
    getFonctions();
    getRoles();
  }, []);

  const fonctionsList = fonctions.map((fonction) => <option key={fonction.id} value={fonction.id} >{fonction.libelle}</option>);
  const rolesList = roles.map((role) => <option key={role.id} value={role.id} >{role.libelle}</option>);
 
  return <Fragment>
    {isLoading ? (
      <div></div>
    ) : (<div className="user">
      <h3>Formulaire modification utilisateur : </h3>
      <form
        className="needs-validation formUser"
        onSubmit={submitForm}
        noValidate
      >
        <MDBRow>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.nom}
              name="nom"
              onChange={changeHandler}
              type="text"
              id="nom"
              label="Nom"
              required
            >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.prenom}
              name="prenom"
              onChange={changeHandler}
              type="text"
              id="prenom"
              label="Prenom"
              required >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.mail}
              name="mail"
              onChange={changeHandler}
              type="text"
              id="mail"
              label="Email"
              required>
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol >
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.tel}
              name="tel"
              onChange={changeHandler}
              type="tel"
              id="tel"
              label="Telephone"
              required >
              <div className="invalid-feedback">Looks good!</div>
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="8" className="mb-6">
            <MDBInput
              valueDefault={user.dateDeNaissance}
              name="dateDeNaissance"
              onChange={changeHandler}
              type="date"
              id="adresse"
              label="Date de naissance"
              required>
              <div className="invalid-feedback">Looks good!</div>
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="8" className="mb-6">
            <MDBInput
              valueDefault={user.adresse}
              name="adresse"
              onChange={changeHandler}
              type="text"
              id="adresse"
              label="Adresse"
              required>
              <div className="invalid-feedback">Looks good!</div>
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.authentification.login}
              name="login"
              onChange={changeHandler}
              type="text"
              id="login"
              label="Login"
              disabled
            >
              <div className="invalid-feedback">Looks good!</div>
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.authentification.mdp}
              name="mdp"
              onChange={changeHandler}
              type="password"
              id="mdp"
              label="Mot de passe"
              required
            >
              <div className="invalid-feedback">Looks good!</div>
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="4" className="mb-3">
            <label
              htmlFor="fonction"
              className="grey-text"
            >
              Fonction :
              </label>
            <select className="browser-default custom-select" name="fonction" onChange={changeHandler}>
              <option>Votre Fonction</option>
              {fonctionsList}
            </select>
            <div className="invalid-tooltip">
              Please provide a valid zip.
              </div>
            <div className="valid-tooltip">Looks good!</div>
          </MDBCol>

          <MDBCol md="4" className="mb-3">
            <label htmlFor="role" className="grey-text">
              Role :
              </label>
            <select className="browser-default custom-select" name="role" onChange={changeHandler}>
              <option>Role</option>
              {rolesList}
            </select>
            <div className="invalid-tooltip">
              Please provide a valid zip.
              </div>
            <div className="valid-tooltip">Looks good!</div>
          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="4" className="mb-3" ></MDBCol>
          <MDBCol>
            <MDBBtn type="submit">Valider</MDBBtn>
          </MDBCol>
        </MDBRow>
      </form>
    </div>)}
  </Fragment>
}