import React, { useEffect, useState } from 'react';
import {
    MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
    MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon
} from 'mdbreact';
import { BrowserRouter, Link as Router } from 'react-router-dom';
import '../css/header.css'

export default function Header() {
    const [collapse, setCollapse] = useState(false);
    const [wideEnough, setWideEnough] = useState(false);




    function onClick() {
        setCollapse(!collapse);
    }


    return (
        <div className="header">
            <header>
                <Router>
                <MDBNavbar  fixed="top" dark expand="md" className="header">
                        <MDBNavbarBrand href="/">
                      
                        <img className="imgHeader" src="/img/logo.png" alt="logo" />
                                Partie salle
                        </MDBNavbarBrand>
                        {!wideEnough && <MDBNavbarToggler onClick={onClick} />}
                        <MDBCollapse isOpen={collapse} navbar>
                            <MDBNavbarNav left>
                                <MDBNavItem>
                                    <MDBDropdown>
                                        <MDBDropdownToggle nav caret>
                                            <div className="d-none d-md-inline black-text">Salle</div>
                                        </MDBDropdownToggle>
                                        <MDBDropdownMenu className="dropdown-default ">
                                            <MDBNavLink to="/salle" className="black-text">Liste salle</MDBNavLink>
                                            <MDBNavLink  to="/creationsalle" className="black-text">Nouvelle salle</MDBNavLink>
                                        </MDBDropdownMenu>
                                    </MDBDropdown>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBDropdown>
                                        <MDBDropdownToggle nav caret>
                                            <div className="d-none d-md-inline black-text">Reservation</div>
                                        </MDBDropdownToggle>
                                        <MDBDropdownMenu className="dropdown-default">
                                        <MDBNavLink to="#" className="black-text">Liste reservation</MDBNavLink>
                                            <MDBNavLink  to="/creationreservation" className="black-text">Nouvelle Reservation</MDBNavLink>
                                           
                                        </MDBDropdownMenu>
                                    </MDBDropdown>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="/batiment" className="black-text">Batiment</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="/typesalle" className="black-text">Type salle</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink to="/typemateriel" className="black-text">Type materiel</MDBNavLink>
                                </MDBNavItem>

                            </MDBNavbarNav>
                        </MDBCollapse>
                    </MDBNavbar>
                </Router>


            </header>
        </div>
    );
}

