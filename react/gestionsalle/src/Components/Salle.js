import React, { useState } from 'react';
import { MDBCard,MDBCol,MDBCardImage,MDBCardBody,MDBCardTitle,MDBCardText,MDBBtn,MDBCardGroup } from "mdbreact";
import $ from 'jquery';
import imgSalle from '../img/salle.png'

export default function Salle({ salle}) {

    //const [user, setUser] = useState(user1);



 /*   function editable() {
       document.location.href ="/admin/user/modif/"+ user.authentification.login
    }


    async function supprimer() {
      if (window.confirm("voullez vous supprimer le post ")) {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/personne/delete/` + user.authentification.login, {
            method: "DELETE"
        }).then(response => { return response.json(); })
            .catch(error => { console.log(error); });
        alert("utilisatuer supprime ");
        suppression();
    }

    }*/

    


    return   <MDBCol className="tabListeSalle" >
    <MDBCard className="cardListeSalle">
      <MDBCardImage className="img-fluid" src={imgSalle} waves />
      <MDBCardBody>
        <MDBCardTitle>{salle.nom}</MDBCardTitle>
        <MDBCardText>
          {salle.typeSalle.libelle}<br/>
          Capacite : {salle.capacite}
        </MDBCardText>
        <MDBBtn onClick={() => document.location.href ="/salle/"+ salle.idSalle}>Détails</MDBBtn>
      </MDBCardBody>
    </MDBCard>
  </MDBCol>
}