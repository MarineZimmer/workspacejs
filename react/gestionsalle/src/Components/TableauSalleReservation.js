import React, { useState, useEffect } from 'react';
import { MDBTable, MDBTableBody, MDBTableHead,MDBBtn,MDBIcon, MDBRow,MDBCardGroup } from 'mdbreact';
import Salle from './SalleReservation';

export default function TableauSalle({salles}) {
    

    const salleListe = salles.map((salle) => <Salle key={salle.id} salle={salle}/>);

    return <div class="listeSalle">
        <h3>Liste des salles disponibles :</h3> 
        <MDBCardGroup>
        {salleListe}</MDBCardGroup>
        </div>;
}