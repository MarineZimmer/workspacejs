import React, { useState, useEffect, useRef,Fragment } from 'react';
import $ from 'jquery'
import{ MDBCard, MDBCardHeader, MDBCardBody, MDBDataTable,MDBTable, MDBTableBody, MDBTableHead,MDBBtn,MDBIcon  }from "mdbreact";
import LigneTableauDivers from './LigneTableauDivers'

export default function Gestion({type,refresh,setRefresh}) {

    
    const [typeMateriels, setTypeMateriels] = useState([]);
    const [newTypeMateriel, setNewTypeMateriel] = useState({ "libelle": "" });
    const [isLoading, setIsLoading] = useState(true);


    const getTypeMateriels = async () => {
       await fetch(`${process.env.REACT_APP_API_URL}`+"/"+type)
            .then(response => response.json())
            .then(data => setTypeMateriels(data))
            .catch(error => console.log(error))
    }


    function changeHandler(event) {
        setNewTypeMateriel(event.target.value)
    };

    async function submitForm(event) {
        event.preventDefault();
        let response = await fetch(`${process.env.REACT_APP_API_URL}/typeMateriel/new`, {
            method: "POST",
            body: JSON.stringify(newTypeMateriel),
            headers: {
                //"Content-Type": "multipart/form-data" }
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8"
            }
        })

    }
    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            await getTypeMateriels();
            setIsLoading(false);
        }
        fetchData();
        console.log(titre)

    }, [refresh,type]);



    const tabTypeMateriel = typeMateriels.map((objet) => <LigneTableauDivers key={objet.id} rowObjet={objet} type={type} setRefresh={setRefresh}/>);

    var titre = function(){
        console.log("toto")
        console.log(type)
        if(type=="typemateriel"){
            return "Type Materiel"
        }else  if(type=="batiment"){
            return "Batiment"
        }else if(type=="typesalle"){
            return "Type Salle"
        }
    }();

    return (<Fragment>
        {isLoading ? (
            <div></div>
        ) : (<div>
             < MDBTable striped hover className="tabDivers">
        <MDBTableHead><tr>
                <th><h3>{titre}</h3></th>
                <th></th>
               </tr></MDBTableHead>
                <MDBTableBody>{tabTypeMateriel}</MDBTableBody></MDBTable>
        </div>)
        }</Fragment>);

}