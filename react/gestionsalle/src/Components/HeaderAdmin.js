import React, { useEffect, useState } from 'react';
import {
    MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
    MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon
} from 'mdbreact';
import { BrowserRouter, Link as Router } from 'react-router-dom';
import '../css/header.css'


export default function Header() {
    const [collapse, setCollapse] = useState(false);
    const [wideEnough, setWideEnough] = useState(false);



    function onClick() {
        setCollapse(!collapse);
    }


    return (
        <div className="header">
            <header>
                <Router>
                    <MDBNavbar  fixed="top" dark expand="md" className="header">

                        <MDBNavbarBrand>

                         
                                <img className="imgHeader" src="/img/logo.png" alt="logo" />
                                Partie administrateur

                        </MDBNavbarBrand>
                        {!wideEnough && <MDBNavbarToggler onClick={onClick} />}
                        <MDBCollapse isOpen={collapse} navbar>
                            <MDBNavbarNav left>

                                <MDBNavItem>
                                    <MDBNavLink className="black-text" to="/admin/user">Utilisateurs</MDBNavLink>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBNavLink className="black-text" to="/admin/user/new">Nouveau utilisateur</MDBNavLink>
                                </MDBNavItem>
                            </MDBNavbarNav>
                        </MDBCollapse>
                    </MDBNavbar>
                </Router>


            </header>
        </div>
    );
}

