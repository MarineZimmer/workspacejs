import React, { useState, useEffect, useRef } from 'react';
import $ from 'jquery'
import { MDBRow, MDBCol, MDBBtn, MDBInput, MDBFormInline } from "mdbreact";

export default function FormCreationSalle(props) {
  const [salle, setSalle] = useState({
    "idSalle": "0",
    "numero": "",
    "nom": "",
    "surface": "",
    "capacite": "",
    "etage": "",
    "actif": true,
    "typeSalle": {
      "id": 1,
      "libelle": ""
    },
    "listeReservation":[],
    "listeMateriel": [] ,
    "batiment": {
      "id": "",
      "nom": ""
    }
  });
  const [typeSalles, setTypeSalles] = useState([]);
  const [typeMateriels, setTypeMateriels] = useState([]);
  const [batiments, setBatiments] = useState([]);


  const getypeSalle = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/typesalle`)
      .then(response => response.json())
      .then(data => setTypeSalles(data))
      .catch(error => console.log(error))
  }

  const getTypeMateriel = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/typemateriel`)
      .then(response => response.json())
      .then(data => setTypeMateriels(data))
      .catch(error => console.log(error))
  }

  const getBatiment = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/batiment`)
      .then(response => response.json())
      .then(data => setBatiments(data))
      .catch(error => console.log(error))
  }

  function changeHandler(event) {
    let salleTemp = salle
    if (event.target.name === "typeSalle") {
      salleTemp.typeSalle = typeSalles.find(e => e.id == event.target.value)
    } else if (event.target.name === "batiment") {
      salleTemp.batiment = batiments.find(e => e.id == event.target.value)
    } else if (event.target.name === "typeMateriel") {
      let typeMateriel = typeMateriels.find(e => e.id == event.target.id)
      var  materiel={};
        materiel.typeMateriel=typeMateriel;
        materiel.quantite=event.target.value;
        materiel.id=0;
    salleTemp.listeMateriel=salleTemp.listeMateriel.filter((m)=>{
      return m.typeMateriel.id!=materiel.typeMateriel.id
    })
    salleTemp.listeMateriel.push(materiel)
    }else  if(event.target.name === "actif"){
      console.log(event.target.value)
      salleTemp.actif=event.target.value
    }else {
      salleTemp[event.target.name] = event.target.value;
    }
    
    setSalle(salleTemp)
    console.log(salle)
  };



  async function submitForm(event) {
    event.preventDefault();
    let response = await fetch(`${process.env.REACT_APP_API_URL}/salle`, {
      method: "POST",

      body: JSON.stringify(salle),

      headers: {
        //"Content-Type": "multipart/form-data" }
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8"
      }
    })

    let responseData = await response.json();
    console.log(response.ok);
    
    if (responseData) {
      alert("ok")
      
    } else {
      alert("ko")
    }
    console.log(responseData);
  };

  useEffect(() => {
    getTypeMateriel();
    getypeSalle();
    getBatiment();


  }, []);



  const batimentsList = batiments.map((batiment) => <option key={batiment.id} value={batiment.id} >{batiment.libelle}</option>);
  const typesallesList = typeSalles.map((typeSalle) => <option key={typeSalle.id} value={typeSalle.id} >{typeSalle.libelle}</option>);
  const typeMaterielsList = typeMateriels.map((typeMateriel) =>
    <MDBRow key={typeMateriel.id}> <div className="form-group">
      <label htmlFor="typeMateriel">{typeMateriel.libelle}</label>
      <input
        type="number"
        className="form-control"
        name="typeMateriel"
        id={typeMateriel.id}
        onChange={changeHandler}
        

      />
    </div></MDBRow>);




  return <div className="newSalle">
    <h3 >Formulaire nouvelle salle : </h3>
    <form
      className="needs-validation formSalle"
      onSubmit={submitForm}
      noValidate
    >
      <MDBRow>
        <MDBCol md="4" className="mb-3">
          <MDBRow >
            <MDBInput
              valueDefault={salle.nom}
              name="nom"
              onChange={changeHandler}
              type="text"
              id="nom"
              label="Nom"
              required
            >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBRow>

          <MDBRow>
            <label
              htmlFor="typeSalle"
              className="grey-text"
            >
              Type de salle :
              </label>
            <select className="browser-default custom-select" name="typeSalle" onChange={changeHandler}>
              <option>Type de salle</option>
              {typesallesList}
            </select>
            <div className="invalid-tooltip">
              Please provide a valid zip.
              </div>
            <div className="valid-tooltip">Looks good!</div>
          </MDBRow>
          <MDBRow>
            <label
              htmlFor="typeSalle"
              className="grey-text"
            >
              Batiment :
              </label>
            <select className="browser-default custom-select" name="batiment" onChange={changeHandler}>
              <option>Choix batiment</option>
              {batimentsList}
            </select>
            <div className="invalid-tooltip">
              Please provide a valid zip.
              </div>
            <div className="valid-tooltip">Looks good!</div>
          </MDBRow>
          <MDBRow >
            <MDBInput
              valueDefault={salle.numero}
              name="numero"
              onChange={changeHandler}
              type="number"
              id="numero"
              label="numero"
              required
            >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBRow>
          <MDBRow >
            <MDBInput
              valueDefault={salle.etage}
              name="etage"
              onChange={changeHandler}
              type="number"
              id="etage"
              label="etage"
              required
            >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBRow>
          <MDBRow >
            <MDBInput
              valueDefault={salle.capacite}
              name="capacite"
              onChange={changeHandler}
              type="number"
              id="capacite"
              label="capacite"
              required
            >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBRow>
          <MDBRow >
            <MDBInput
              valueDefault={salle.capacite}
              name="surface"
              onChange={changeHandler}
              type="number"
              id="superficie"
              label="superficie"
              required
            >
              <div className="valid-feedback">Looks good!</div>
            </MDBInput>
          </MDBRow>
          <MDBRow> Salle active/inactive : </MDBRow>
          <MDBRow>

            <MDBFormInline>

              {/*onclick , checked*/}

              <MDBInput
               onChange={changeHandler}
                label='active'
                type='radio'
                id='active'
                name="actif"
                value="true"
                containerClass='mr-5'
              />
              <MDBInput
               onChange={changeHandler}
                label='inactive'
                type='radio'
                id='inactive'
                name="actif"
                value="false"

                containerClass='mr-5'
              />
            </MDBFormInline>
          </MDBRow>
        </MDBCol>
        <MDBCol md="1" className="mb-3">
        </MDBCol>
        <MDBCol md="4" className="mb-3">
          <br />
          <h4>Liste de matériel de la salle :</h4>
          <br />
          {typeMaterielsList}
        </MDBCol>
      </MDBRow>
      <MDBRow>
        <MDBCol md="4" className="mb-3" ></MDBCol>
        <MDBCol>
          <MDBBtn type="submit">Valider</MDBBtn>
        </MDBCol>
      </MDBRow>
    </form>
  </div>
}