import React, { useState } from 'react';
import { MDBIcon } from "mdbreact";
import $ from 'jquery';

export default function User({ user1, suppression}) {

    const [user, setUser] = useState(user1);



    function editable() {
       document.location.href ="/admin/user/modif/"+ user.authentification.login
    }


    async function supprimer() {
      if (window.confirm("voullez vous supprimer le post ")) {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/personne/delete/` + user.authentification.login, {
            method: "DELETE"
        }).then(response => { return response.json(); })
            .catch(error => { console.log(error); });
        alert("utilisatuer supprime ");
        suppression();
    }

    }

    


    return <tr>
        <td>{user.authentification.login}</td>
        <td>{user.nom}</td>
        <td>{user.prenom}</td>
        <td>{user.mail}</td>
        <td>{user.tel}</td>
        <td>{user.fonction.libelle}</td>
        <td>{user.role.libelle}</td>
        <td onClick={editable}><MDBIcon icon="pencil-alt" /></td>
        <td onClick={supprimer}><MDBIcon far icon="trash-alt" /></td></tr>
}