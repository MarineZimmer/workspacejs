import React, { useState, useEffect, useRef } from 'react';
import $ from 'jquery'
import { MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";

export default function FormCreationDivers({ type, setRefresh, refresh }) {
    const [objet, setObjet] = useState({
        "id": 0,
        "libelle": ""
    });

    const refInput = useRef(null);


    function changeHandler(event) {
        let objetTemp = objet
        objet.libelle = event.target.value;
        setObjet(objetTemp)
        console.log(objet)
    };



    async function submitForm(event) {
        event.preventDefault();
        console.log(objet)
        let response = await fetch(`${process.env.REACT_APP_API_URL}/` + type, {
            method: "POST",
            body: JSON.stringify(objet),
            headers: {
                //"Content-Type": "multipart/form-data" }
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8"
            }
        })

        let responseData = await response.json();
        console.log(response.ok);
        if (responseData) {
            alert("ok")
        } else {
            alert("ko")
        }
        let objetTemp = objet;
        objet.libelle = "";
        setObjet(objetTemp)
        console.log(objet);
        refInput.current.value = "toto";
        $("#libelle").val('');
        console.log($("#libelle"));
        setRefresh(true);
    };

    useEffect(() => {
        console.log("useEffect creation")
    }, [refresh]);


    var titre = function(){
        console.log("toto")
        console.log(type)
        if(type=="typemateriel"){
            return "Type Materiel"
        }else  if(type=="batiment"){
            return "Batiment"
        }else if(type=="typesalle"){
            return "Type Salle"
        }
    }();


    return <div className="newDivers">
        <h3>Nouveau {titre}  : </h3>
        <form
            className="needs-validation "
            onSubmit={submitForm}
            noValidate
        >
            <MDBRow >
                <MDBCol md="4" className="mb-3">
                <MDBInput
              name="nom"
              onChange={changeHandler}
              type="text"
              id="libelle"
              label= {titre}
              required
            />

                   
                </MDBCol>
                <MDBCol md="4" className="mb-3">
                    <br></br>
                    <MDBBtn type="submit">Valider</MDBBtn>
                </MDBCol>
            </MDBRow>
        </form>
    </div>
}