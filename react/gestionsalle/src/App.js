import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/App.css';
import FormCreationUser from './Pages/AdminNewUtilisateur'
import FormModificationUser from './Pages/AdminModifUtilisateur'
import Login from './Components/Login';
import LoginAdmin from './Components/LoginAdmin';
import ListeUsers from './Pages/AdminListeUtilisateurs';
import ListeSalle from './Pages/ListeSalle';
import HeaderAdmin from './Components/HeaderAdmin';
import TypeMateriel from './Pages/TypeMateriel';
import Batiment from './Pages/Batiment';
import TypeSalle from './Pages/TypeSalle';
import CreationSalle from './Pages/CreationSalle';
import CreationReservation from './Pages/NewReservation';
import SalleDetails from './Pages/SalleDetails';

function App() {
  return (
     
     <Router>
     <div>
      

       {/* A <Switch> looks through its children <Route>s and
           renders the first one that matches the current URL. */}
       <Switch>
       
         <Route path="/admin/user/modif/:login">
           <FormModificationUser />
         </Route>
         <Route path="/admin/user/new">
           <FormCreationUser />
         </Route>
         <Route path="/admin/user">
           <ListeUsers />
         </Route>
         <Route path="/admin" >
           <LoginAdmin />
           </Route>
           <Route path="/salle/:id" >
           <SalleDetails />
         </Route>
         <Route path="/salle" >
           <ListeSalle />
         </Route>
        
         <Route path="/typemateriel" >
         <Batiment type="typemateriel" />
         </Route>
         <Route path="/batiment" >
           <Batiment type="batiment" />
         </Route>
         <Route path="/typesalle" >
         <Batiment type="typesalle" />
         </Route>
         <Route path="/creationsalle" >
         <CreationSalle/>
         </Route>
         <Route path="/creationreservation" >
         <CreationReservation/>
         </Route>
         <Route path="/" >
           <Login />
         </Route>
       </Switch>
     </div>
   </Router>
  );
}

export default App;
