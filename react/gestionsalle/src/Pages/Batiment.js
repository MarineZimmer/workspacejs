import React,{useState,useEffect} from 'react';
import Header from '../Components/Header'
import TableauDivers from '../Components/TableauDivers'
import FormCreationDivers from '../Components/FormCreationDivers'
import { MDBIframe } from 'mdbreact';
export default function ListeSalle({type}){

    const [refresh,setRefresh]=useState(false);

    useEffect(()=>{
        console.log("refresh")
    },[refresh])

    const modif = function(){
        setRefresh(!refresh)
    }

    return <div>
        <Header/>
        <TableauDivers type={type} refresh={refresh} setRefresh={modif} />
        <FormCreationDivers className="newSalle" type={type} setRefresh={modif} refresh={modif}/>
    </div>
}