import React from 'react';
import Header from '../Components/HeaderAdmin'
import ListeUsers from '../Components/ListeUser'
export default function AdminListeUtilisateur(){

    return <div>
        <Header/>
        <ListeUsers/>
    </div>
}