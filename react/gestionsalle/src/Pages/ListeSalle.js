import React from 'react';
import Header from '../Components/Header'
import TableauSalle from '../Components/TableauSalle'
import Filtre from '../Components/FiltreListeSalle'
import { MDBRow, MDBCol } from 'mdbreact';
export default function ListeSalle() {

    return <div>
        <Header />
        <MDBRow>
            <MDBCol md="2" className="mb-3">
                <Filtre />
            </MDBCol>
            <MDBCol md="4" className="mb-3">
                <TableauSalle />
            </MDBCol>
        </MDBRow>
    </div>
}