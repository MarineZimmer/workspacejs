import React from 'react';
import Header from '../Components/HeaderAdmin'
import ModifUser from '../Components/FormModificationUser'
export default function AdminListeUtilisateur(){

    return <div>
        <Header/>
        <ModifUser/>
    </div>
}