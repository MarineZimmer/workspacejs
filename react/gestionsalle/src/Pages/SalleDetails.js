import React from 'react';
import Header from '../Components/Header'
import SalleDetails from '../Components/SalleDetails'
export default function AdminListeUtilisateur(){

    return <div>
        <Header/>
        <SalleDetails/>
    </div>
}