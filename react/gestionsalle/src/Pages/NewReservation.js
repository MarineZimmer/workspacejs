import React, { useState } from 'react';
import { MDBCol, MDBRow} from 'mdbreact';
import Header from '../Components/Header'
import FormNewReservation from '../Components/FormCreationReservation'
import ListeSalleDispo from '../Components/TableauSalleReservation'
export default function AdminListeUtilisateur(){

    const[salles,setSalles]=useState([]);

    async function rechercheSalle(reservation){
        console.log(reservation)
       await  fetch(`${process.env.REACT_APP_API_URL}/salledispo`,{
        method: "POST",
        body: JSON.stringify(reservation),
        headers: {
          'Accept': 'application/json',
          "Content-type": "application/json; charset=UTF-8"
        }
      })
      .then(response=>response.json())
      .then(data=>setSalles(data))
      .catch(error => console.log(error));
      console.log(salles)
    }

    return <div>
        <Header/>
        <MDBRow>
        <MDBCol md="3" className="mb-3">
        <FormNewReservation rechercheSalle={rechercheSalle}/>
        </MDBCol>
        <MDBCol md="6" className="mb-3">
        <ListeSalleDispo salles={salles}/>
        </MDBCol>
        </MDBRow>
    </div>
}