import React from 'react';
import logo from './logo.svg';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './App.css';
import  TodoList from './Components/TodoList'

function App() {
  return (
    <div className="container">
     <TodoList/>
    </div>
  );
}

export default App;
