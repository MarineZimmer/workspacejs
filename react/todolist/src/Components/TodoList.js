import React, { useState, useEffect } from 'react';
import { MDBTable, MDBTableBody } from "mdbreact";
import Todo from './Todo'
import $ from 'jquery'

export default function TodoList() {

    const [todos, setTodos] = useState([{ id: 1, texte: "todo1" }, { id: 2, texte: "todo2" }, { id: 3, texte: "todo3" }, { id: 4, texte: "todo4" }]);
    const [id, setId] = useState(5);
    const [suppr, setSuppr] = useState(false);


    function ajout(event) {
        event.preventDefault();
        if ($("#todo").val() != "") {
            setTodos([...todos, { id: id, texte: $("#todo").val() }]);
            setId(id + 1);
            $("#todo").val("");
        }
    }

    function supprTodo() {
        setSuppr(!suppr)
    }

    useEffect(() =>{
    return window.confirm("voullez vous quitter");
},[])



    
    const todoList = todos.map((todo) => <Todo key={todo.id} todo={todo} setTodos={setTodos} todos={todos} suppr={supprTodo} pos={todos.indexOf(todo)} />);

    return <div className="container">
        <h2>Todo List :</h2>
        <form onSubmit={ajout}>
            <div className="form-group row">
                <div class="col-sm-8">
                    <input type="text" id="todo" className="form-control" placeholder="ajout todo" />
                </div>
                <div class="col-sm-4">
                    <input type="submit" className="form-control" />
                </div>
            </div>
        </form>

        <MDBTable striped bordered hover>

            <MDBTableBody>{todoList}</MDBTableBody>
        </MDBTable >
    </div>


}