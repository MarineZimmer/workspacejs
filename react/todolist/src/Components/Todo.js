import React, { useState, useEffect, useRef } from 'react';
import { MDBIcon } from "mdbreact";
import $ from 'jquery'

export default function Todo(props) {

    const [todo, setTodo] = useState(props.todo);
    const refTodo = useRef(null);
    const refChekBox = useRef(null);


    function modifier() {
        refTodo.current.setAttribute("disabled","true");
        setTodo({ id: todo.id, texte: refTodo.current.value })
    }

    function editable() {
        refTodo.current.removeAttribute("disabled");
        refTodo.current.focus();
    }

    useEffect(() => {
        const tabTodo = props.todos;
        tabTodo[props.pos] = todo;
        props.setTodos(tabTodo);

    }, [todo])

    function supprimer() {
        const tabTodo = props.todos;
        tabTodo.splice(props.pos, 1);
        props.setTodos(tabTodo)
        props.suppr();
    }

    function checked(){
        $(refTodo.current).toggleClass("barre");
    }


    return <tr >
        <td><div className="form-check"><input type="checkbox" className="form-check-input" ref={refChekBox} onChange={checked}/></div></td>
        <td><input type="text" id={todo.id} className="todo" defaultValue={todo.texte} ref={refTodo} disabled onBlur={modifier} /></td>
        <td onClick={editable}><MDBIcon icon="pencil-alt" /></td>
        <td onClick={supprimer}><MDBIcon far icon="trash-alt" /></td>


    </tr>
}