import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './App.css';
import Users from './Pages/Users';
import Posts from './Pages/Posts';
import Comments from './Pages/Comments';
import NewPost from './Pages/NewPost';
import ModifPost from './Pages/ModifPost';

function App() {
  return (
    <Router >
      
     <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">

       
        <div >
          <ul className="navbar-nav ">

            <li className="nav-item">
              <Link className="nav-link" to="/">Liste Users</Link>
            </li>
          </ul>
        </div>
      </nav>
  
      <div  className="container main"  >

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/posts/:id">
            <Posts />
          </Route>
          <Route path="/comments/:idUser/:id">
            <Comments />
          </Route>
          <Route path="/newPost/:idUser">
            <NewPost />
          </Route>
          <Route path="/modifPost/:idUser/:idPost">
            <ModifPost />
          </Route>
          <Route path="/">
            <Users />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
