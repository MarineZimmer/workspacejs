import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import '../App.css';

function Posts() {
    let { id } = useParams();
    const [posts, setPosts] = useState([]);
    const [suppr, setSuppr] = useState(false);

    const getPosts = () => {
        fetch(`${process.env.REACT_APP_API_URL}/posts?userId=${id}`)
            .then(response => { return response.json(); })
            .then(data => setPosts(data))
            .catch(error => { console.log(error); });
    }

    useEffect(() => {
        getPosts();
    }, [id, suppr]);

    async function deletePost(id) {

        if (window.confirm("voullez vous supprimer le post ")) {
            let response = await fetch(`${process.env.REACT_APP_API_URL}/posts/` + id, {
                method: "DELETE"
            }).then(response => { return response.json(); })
                .catch(error => { console.log(error); });
            alert("post supprime ");
            setSuppr(!suppr);
        }
    }

    function newPost(event) {
        document.location.href = `/newPost/${id}`;
    }

    const postsList = posts.map((post) => <tr className="select" key={post.id}  >
        <td onClick={(e) => document.location.href = `/comments/${post.userId}/${post.id}`}>{post.title}</td>
        <td onClick={(e) => document.location.href = `/comments/${post.userId}/${post.id}`}>{post.body}</td>
        <td onClick={(e) => document.location.href = `/modifPost/${post.userId}/${post.id}`}><i className="fas fa-pencil-alt"></i></td>
        <td onClick={(e) => deletePost(post.id)}><i className="far fa-trash-alt"></i></td>
    </tr>);

    return <div>
        <div className="ajout">
            <button type="button" className="btn btn-outline-secondary btn-sm" onClick={newPost}>New Post</button>
        </div>
        <div className="test">

            <table className="table table-bordered">
                <thead><tr><th>Title</th><th colSpan="3">Post</th></tr></thead>
                <tbody>{postsList}</tbody></table>
        </div>
    </div>;
}

export default Posts;
