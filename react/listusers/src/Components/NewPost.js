import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import $ from 'jquery';


function NewPost() {
  let { idUser } = useParams();

  async function envoiPost(event) {
    event.preventDefault();
    var data = {
      title: $("#title").val(),
      body: $("#body").val(),
      userId: idUser
    }

    let responseData = await fetch(`${process.env.REACT_APP_API_URL}/posts`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then(response => response.json())
      .catch(error => console.log(error));

    alert("post créé id : " + responseData["id"] + "\n titre : " + responseData["title"] + "\n post : " + responseData["body"] + "\n userId : " + responseData["userId"]);
    document.location.href = `/posts/${idUser}`;
  }

  return <form id="form" method="POST" action="#" onSubmit={envoiPost}>
    <h3>Nouveau post : </h3>
    <div className="form-group">
      <label htmlFor="title">Titre : </label>
      <input type="text" className="form-control" id="title" name="title" placeholder="titre" />
    </div>
    <div className="form-group">
      <label htmlFor="boby">Message : </label>
      <textarea className="form-control" id="body" name="body" rows="6"></textarea>
    </div>
    <div className="form-group">
      <input type="submit" className="btn btn-outline-secondary" />
    </div>
  </form>
}

export default NewPost;
