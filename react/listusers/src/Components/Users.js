import React, { useState, useEffect } from 'react';

function Users() {
    const [users, setUsers] = useState([]);
    const [todoList, setTodoList] = useState([]);

    const getUsers = async () => {
        const test = await  fetch(`${process.env.REACT_APP_API_URL}/todos`)
        console.log(test)
         await  fetch(`${process.env.REACT_APP_API_URL}/todos`)
         .then(response => {
            return response.json();
        })
         .then(data=>setTodoList(data))
         .catch(error => {
             console.log(error);
         }
         ); 
         console.log(todoList)

       await fetch(`${process.env.REACT_APP_API_URL}/todos`)
            .then(response => {
                return response.json();
            })
            .then(data=>setUsers(data))
            .catch(error => {
                console.log(error);
            }); 
    }

    useEffect(() => {
        getUsers();
    }, []);

    
    const usersList = todoList.map((user) => <tr className="select" key={user.id} onClick={(e) => document.location.href=`/posts/${user.id}` } >
        <td>{user.todo}</td><td>{user.todo}</td><td>{user.todo}</td></tr>);

    return <div>
        <h1>Liste des Utilisateurs :</h1> 
        <table className="table table-bordered">
            <thead ><tr><th>Name</th><th>Username</th><th>Email</th></tr></thead>
                <tbody>{usersList}</tbody></table>
        </div>;
}

export default Users;
