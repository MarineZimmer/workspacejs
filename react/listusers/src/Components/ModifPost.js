import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import $ from 'jquery';


function NewPost() {
  let { idUser, idPost } = useParams();

  const [post,setPost] = useState({title:"",body:""});

  const getPost = () =>{
    fetch(`${process.env.REACT_APP_API_URL}/posts/${idPost}`)
          .then(response=>response.json())
          .then(data=>setPost(data))
          .catch(error=>console.log(error))
    }
  useEffect(()=>{
    getPost();
    console.log(post)
  },[])

   async function modifPost(event) {
    event.preventDefault();

    var data = {
      id: idPost,
      title: $("#title").val(),
      body: $("#body").val(),
      userId: idUser
    }

     let responseData =  await fetch(`${process.env.REACT_APP_API_URL}/posts/${idPost}`, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    }).then(response => response.json())
      .catch(error => console.log(error));

    alert("post modifié id : " + responseData["id"] + "\n titre : " + responseData["title"] + "\n post : " + responseData["body"] + "\n userId : " + responseData["userId"]);
    document.location.href = `/posts/${idUser}`;

  }




  return <form id="form" method="PUT" action="#" onSubmit={modifPost}>
    <h3>Modification post : </h3>
    <div className="form-group">
      <label htmlFor="title">Titre : </label>
      <input type="text" className="form-control" id="title" name="title" defaultValue={post.title} />
    </div>
    <div className="form-group">
      <label htmlFor="body">Message : </label>
      <textarea className="form-control" id="body" name="body" rows="6" defaultValue={post.body}></textarea>
    </div>
    <div className="form-group">
      <input type="submit" className="btn btn-outline-secondary" />
    </div>
  </form>
}

export default NewPost;
