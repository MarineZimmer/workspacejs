import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";

function Comments() {
    const [comments, setComments] = useState([]);
    let { idUser, id } = useParams();

    const getComments = () => {
        fetch(`${process.env.REACT_APP_API_URL}/comments?postId=${id}`)
            .then(response => { return response.json(); })
            .then(data => setComments(data))
            .catch(error => { console.log(error); });
    }

    useEffect(() => {
        getComments();
    }, [id]);

    const commentsList = comments.map((comment) => <tr key={comment.id}>
        <td>{comment.email}</td><td>{comment.name}</td><td>{comment.body}</td></tr>);

    return <div>
        <a href={"/posts/" + idUser}>Retour liste des posts</a>
        <h3>Liste des comments du post:</h3>
        <table className="table table-bordered">
            <thead><tr><th>Email</th><th>Name</th><th>Comments</th></tr></thead>
            <tbody>{commentsList}</tbody></table>
    </div>;
}

export default Comments;
