document.getElementById("formCreation").addEventListener("submit", this.valider);

function valider(event) {
	var nom = document.getElementById('nom');
	var prenom = document.getElementById('prenom');
	var email = document.getElementById('email');
	var tel = document.getElementById('tel');
	var adresse = document.getElementById('adresse');
	var date = document.getElementById('dateNaissance');
	var mdp = document.getElementById('mdp');
	document.getElementById('nomErr').innerHTML='';
	document.getElementById('prenomErr').innerHTML='';
	document.getElementById('telErr').innerHTML='';
	document.getElementById('emailErr').innerHTML='';
	document.getElementById('adresseErr').innerHTML='';
	document.getElementById('dateErr').innerHTML='';
	document.getElementById('loginErr').innerHTML='';
	document.getElementById('mdpErr').innerHTML='';
	var retour = true;

	if (nom.value.length == 0) {
		document.getElementById('nomErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	} else if (nom.value.replace(" ", "").length < nom.value.length) {
		document.getElementById('nomErr').innerHTML = 'pas d\'espace dans le nom';
		retour = false;
	} else if (nom.value.includes(',')) {
		document.getElementById('nomErr').innerHTML = 'pas de virgule dans le nom';
		retour = false;
	}
 else if (nom.value.match('[0-9]+')) {
	document.getElementById('nomErr').innerHTML = 'pas de chifre dans le nom';
	retour = false;
}

	if (prenom.value.length == 0) {
		document.getElementById('prenomErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
 else if (prenom.value.replace(" ", "").length < prenom.value.length) {
	document.getElementById('nomErr').innerHTML = 'pas d\'espace dans le prenom';
	retour = false;
} else if (prenom.value.includes(',')) {
	document.getElementById('prenomErr').innerHTML = 'pas de virgule dans le prenom ';
	retour = false;
}
else if (prenom.value.match('[0-9]+')) {
	document.getElementById('prenomErr').innerHTML = 'pas de chifre(s) dans le prenom';
	retour = false;
}
 

	if (email.value.length == 0) {
		document.getElementById('prenomErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
 else if (email.value.replace(" ", "").length < email.value.length) {
	document.getElementById('emailErr').innerHTML = 'pas d\'espace dans le mail';
	retour = false;
} else if (email.value.includes(',')) {
	document.getElementById('emailErr').innerHTML = 'pas de virgule dans le mail';
	retour = false;
}

	
	if (tel.value.length == 0) {
		document.getElementById('telErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
 else if (tel.value.replace(" ", "").length < tel.value.length) {
	document.getElementById('telilErr').innerHTML = 'pas d\'espace dans le telephone';
	retour = false;
} else if (tel.value.includes(',')) {
	document.getElementById('telErr').innerHTML = 'pas de virgule dans le telephone';
	retour = false;
}
else if (tel.value.match('[a-zA-Z]+')) {
	document.getElementById('telErr').innerHTML = 'pas de lettres dans le numero';
	retour = false;
}
	
	if (adresse.value.length == 0) {
		document.getElementById('adresse').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
	
	if (date.value.length == 0) {
		document.getElementById('dateErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
 else if (date.value.replace(" ", "").length < date.value.length) {
	document.getElementById('dateErr').innerHTML = 'pas d\'espace dans la date de naissance';
	retour = false;
} else if (tel.value.includes(',')) {
	document.getElementById('dateErr').innerHTML = 'pas de virgule dans la date de naissance';
	retour = false;
}

	if (login.value.length == 0) {
		document.getElementById('loginErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
 else if (login.value.replace(" ", "").length < login.value.length) {
	document.getElementById('loginErr').innerHTML = 'pas d\'espace dans le login';
	retour = false;
} else if (login.value.includes(',')) {
	document.getElementById('loginErr').innerHTML = 'pas de virgule dans le login';
	retour = false;
}
	
	if (mdp.value.length == 0) {
		document.getElementById('mdpErr').innerHTML = 'veuillez renseigner ce champ';
		retour = false;
	}
 else if (mdp.value.replace(" ", "").length < mdp.value.length) {
	document.getElementById('mdpErr').innerHTML = 'pas d\'espace dans le mot de passe';
	retour = false;
} else if (login.value.includes(',')) {
	document.getElementById('mdpErr').innerHTML = 'pas de virgule dans le mot de passe';
	retour = false;
}
	
	
	
	if (retour) {
		return true;
	} else {
		event.preventDefault();
		return false;
	}
}