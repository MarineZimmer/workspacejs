var perimetre = function(){
    var resultat;
    var calcul=0;
    switch (arguments.length){
        case 0 : resultat = 'erreur pas de parametre';break;
        case 1 : resultat = 'c\'est un carré, perimètre :' + (arguments[0]*4);break;
        case 2 : resultat = 'c\'est un rectangle, perimètre :' + ((arguments[0]+arguments[1])*2);break;
        default : for(i in arguments){
            calcul+=arguments[i];
        }
        resultat = 'c\'est un polygone , perimètre :' + (calcul);break;
    }
    return resultat; 

}

console.log(perimetre());
console.log(perimetre(5));
console.log(perimetre(2,4));
console.log(perimetre(2,3,5,4,6,8));

