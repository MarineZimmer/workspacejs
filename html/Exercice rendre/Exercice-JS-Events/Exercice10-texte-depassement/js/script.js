$("#texte").keydown(function () {
  var nbLettre = $("#texte").val().split(" ").join("").length;
  var mot = $("#texte").val().split(" ");
  var nbMot = 0;
  for (i in mot) {
    if (mot[i].length > 0) {
      nbMot++;
    }
  }

  $("#resultat").html("N° de mots : " + nbMot + "<br>" + "N° de caractères : " + nbLettre);
  if (nbMot > 10 || nbLettre > 100) {
    $("#erreur").html("Dépassement");
    /* $("#texte").attr("disabled","disabled");*/
  } else {
    $("#erreur").html("");
  }
})
