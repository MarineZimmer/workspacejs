$("#textRecopier").keyup(function () {
  var resultat;
  if ($(this).val() != $("#texteVerif").html()) {
    resultat = "Incorrect : ";
    $("#resultat").css("color", "red");
  } else {
    resultat = "Correct : ";
    $("#resultat").css("color", "green");
  }


  $("#resultat").html(resultat + $(this).val());
})