$("#form").submit(function () {
  var sal = $("#salaire").val();
  var codePostal = $("#codePostal").val();
  var regexCP = /[0-9]{5}/;
  var table = $("#table");

  var tr = document.createElement("tr");
  var tdNom = document.createElement("td");
  var tdPrenom = document.createElement("td");
  var tdAge = document.createElement("td");

  $(tdNom).html($("#nom").val());
  $(tdPrenom).html($("#prenom").val());
  $(tdAge).html($("#age").val());

  tr.append(tdNom, tdPrenom, tdAge);
  table.append(tr);
  return false;
})

