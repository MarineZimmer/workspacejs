let nbTrouve = "";
let tentative = 0;

$("#formDifficulte").submit(function (event) {
  event.preventDefault();
  let difficulte = parseInt($("#selDifficulte").val());
  genaration(difficulte);
  initialisation();
  let div = document.createElement("div");
  $(div).html("Je pense à un nombre à : " + nbTrouve.length + "chiffres");
  $("#resultat").append(div);
  $("#jeu").removeAttr("hidden");
  return false;
})

function genaration(difficulte) {
  while (nbTrouve.length < difficulte) {
    let nb = parseInt(Math.random() * 10);
    console.log(nb);
    if (!nbTrouve.includes(nb)) {
      nbTrouve += nb;
    }
  }
}

function initialisation() {
  $("#resultat").children().remove();
  tentative = 0;
  $("#essai").removeAttr("disabled");
  $("#btn").removeAttr("disabled");
}


$("#btn").click(function (event) {
  controle();
})

$("#essai").keyup(function (event) {
  if(event.which === 13){
    controle();
  }
})


function controle() {
  let taureau = 0;
  let vache = 0;
  let essai = $("#essai").val();

  if (essaiValide(essai) ) {
    tentative++;
    for (let i = 0; i < essai.length; i++) {
      if (essai.charAt(i) == nbTrouve.charAt(i)) {
        taureau++;
      } else if (nbTrouve.includes(essai.charAt(i))) {
        vache++;
      }
    }
    let div = document.createElement("div");
    if (taureau === nbTrouve.length) {
      $(div).addClass("gagne");
      $(div).html(tentative + "  : Gagné !!!!, Vous avez trouvé le bon nombre : " + nbTrouve);
      $("#essai").attr("disabled", "");
      $("#btn").attr("disabled", "");

    } else {
      $(div).addClass("perdu");
      $(div).html(tentative + " : " + essai + "  : " + taureau + " taureaux et " + vache + " vaches");
    }
    $("#resultat").prepend(div);
  } else {
    let div = document.createElement("div");
    $(div).addClass("erreur");
    $(div).html("Erreur, Vous devez entrer " + nbTrouve.length + " chiffres différents");
    $("#resultat").prepend(div);
  }
  $("#essai").val("");


}

function essaiValide(essai){
var regex = /^[0-9]+$/;
  if (!essai.match(regex) || essai.length!==nbTrouve.length ){
    return false;
  }
  for(let i=0; i<essai.length;i++){
    for(let j=i+1; j<essai.length;j++){
      if(essai.charAt(i)===essai.charAt(j)){
        return false;
      }
    }
  }
  return true;

}