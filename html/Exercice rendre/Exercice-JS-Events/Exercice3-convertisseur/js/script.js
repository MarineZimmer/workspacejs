$("#euro").keyup(function () {
  var nb = parseFloat($(this).val());
  $("#dollar").val((nb * 1.24).toFixed(2));
})

$("#dollar").keyup(function () {
  var nb = parseFloat($(this).val());
  $("#euro").val((nb / 1.24).toFixed(2));
})