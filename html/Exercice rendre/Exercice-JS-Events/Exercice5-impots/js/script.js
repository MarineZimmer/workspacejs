$("#form").submit(function () {
  var sal = $("#salaire").val();
  var codePostal = $("#codePostal").val();
  var regexCP = /[0-9]{5}/;

  var correct = true;
  if (sal < 0) {
    $("#erreurSal").html("Entrer un salaire positif");
    correct = false;
  } else {
    $("#erreurSal").html("");
  }
  if (!codePostal.match(regexCP)) {
    $("#erreurCP").html("Entrer un code postal valide");
    correct = false;
  } else {
    $("#erreurCP").html("");
  }

  if (correct) {
    if (codePostal.substring(0, 2) == "59") {
      $("#resultat").html("Pas d'impots à payer");
    } else {
      $("#resultat").html("Vous devez : " + sal / 2 + "€");
    }
  }

  return false;
})

