

$("#formAjoutTodo").click(function (event) {
  if($("#ajoutTodo").val() !=""){
  listeTodo($("#ajoutTodo").val());
  $("#ajoutTodo").val("");
  }
})

/*$("#ajoutTodo").blur(function (event) {
  console.log("blur")
  if($("#ajoutTodo").val() !=""){
  listeTodo($("#ajoutTodo").val());
  $("#ajoutTodo").val("");
}
})*/

$("#ajoutTodo").keyup(function (event) {
 
  if(event.which === 13 && $("#ajoutTodo").val() !=""){
  listeTodo($("#ajoutTodo").val());
  $("#ajoutTodo").val("");
  }
})

function listeTodo(todo) {

  let table = $("#tabTodo");

  let tr = document.createElement("tr");

  /*colonne 1 checkbok*/
  let tdCheckTodo = document.createElement("td");
  let checkTodo = document.createElement("input");
  $(checkTodo).attr("type", "checkbox");
 
  $(checkTodo).change(function(){
      $(this).parent().parent().find("input").toggleClass("barre");
  })

  tdCheckTodo.append(checkTodo);



  /*colonne 2 todo*/
  let tdTodo = document.createElement("td");
  let inputTodo = document.createElement("input");
  $(inputTodo).attr("type", "text");
  $(inputTodo).attr("class", "inputTodo");
  $(inputTodo).attr("disabled", "");
  $(inputTodo).val(todo);
  tdTodo.append(inputTodo);

  /*colonne 3 modifier todo*/
  let tdModifTodo = document.createElement("td");
  let modifIcon = document.createElement("i");
  $(modifIcon).attr("class", "fas fa-pencil-alt");
  $(tdModifTodo).click(function (event) {
    let input = $(this).prev().find("input");
    $(input).removeAttr("disabled");
    input.focus();
    $(input).blur(function () {
      $(this).attr("disabled", "");
    })
  })
  tdModifTodo.append(modifIcon);

  /*colonne 4 supprimer todo*/
  let tdSupprTodo = document.createElement("td");
  let supprIcon = document.createElement("i");
  $(supprIcon).attr("class", "far fa-trash-alt");
  $(tdSupprTodo).click(function (event) {
    $(this).siblings().remove();
    $(this).remove();
  })
  tdSupprTodo.append(supprIcon);

  tr.append(tdCheckTodo, tdTodo, tdModifTodo, tdSupprTodo);
  table.append(tr);
}




