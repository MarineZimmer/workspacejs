$("button").click(function (event) {
    console.log(this.value);
    var depT = $("#carre").position().top;
    var depL = $("#carre").position().left;
    switch (this.value) {
        case "haut": depT = $("#carre").position().top - 50; break;
        case "bas": depT = $("#carre").position().top + 50; break;
        case "droite": depL = $("#carre").position().left + 50; break;
        case "gauche": depL = $("#carre").position().left - 50; break;
    }
    $("#carre").css("top", depT > 0 ? depT : 0);
    $("#carre").css("left", depL > 0 ? depL : 0);
    return false;
})

