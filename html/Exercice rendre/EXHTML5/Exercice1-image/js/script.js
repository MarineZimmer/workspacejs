$("#formCouleur").change(function (event) {
    $("#image").css("background-color", $("#couleur").val());
    return false;
})

$("#formDataCouleur").change(function (event) {
    $("#image").css("background-color", $("#dataCouleur").val());
    return false;
})

$("#formTaille").submit(function (event) {
    $("#image").css("width", $("#largeur").val());
    $("#image").css("height", $("#hauteur").val());
    return false;
})

$("#formPosition").submit(function (event) {
    $("#main").css("position", "absolute");
    $("#main").css("top", $("#axeX").val() + "px");
    $("#main").css("left", $("#axeY").val() + "px");
    return false;
})

