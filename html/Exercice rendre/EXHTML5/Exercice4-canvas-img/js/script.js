fonctionCanvas = function () {
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  var image = document.getElementById("source");

  ctx.drawImage(image, 0, 0);
  ctx.fillStyle = 'black';
  ctx.fillRect(123, 38, 20, 20);

  ctx.fillStyle = "rgba(255, 255, 255, 0.5)";
  ctx.beginPath();
  ctx.lineWidth = "1";
  ctx.arc(28, 57, 11, 0, 2 * Math.PI);
  ctx.fill();
}();




