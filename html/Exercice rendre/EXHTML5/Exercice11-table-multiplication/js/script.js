


var n = function () {
  var valeurEntre;
  var nb;
  do {
    valeurEntre = prompt('Entrer un nombre entier : ');
    nb = parseInt(valeurEntre);
  } while (valeurEntre != nb);
  var table = creationTable(nb);
  var elt = $("#main");
  elt.append(table);
  return nb;
}();


function creationTable(nb) {

  var table = document.createElement("table");

  var caption = document.createElement("caption");
  caption.innerHTML = "Table de multiplication de " + nb;

  var tr = document.createElement("tr");
  var th1 = document.createElement("th");
  var th2 = document.createElement("th");


  th1.innerHTML = "operation";
  th2.innerHTML = "resultat";

  tr.append(th1, th2);
  table.append(caption, tr);

  for (i = 1; i <= 10; i++) {
    var tr = document.createElement("tr");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");

    td1.innerHTML = nb + " * " + i + " = ";
    td2.innerHTML = nb * i;

    tr.append(td1, td2);
    table.append(tr);

  }
  return table;
}

tableMultiplication = function () {
  var elt = $("#table");
  for (var j = 1; j <= 10; j++) {
    var table = creationTable(j);
    elt.append(table);
  }
}();