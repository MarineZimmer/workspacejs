
var chrono = 0;
var interval;
var actif = false;

$("#start").click(function (event) {
  if(!actif){
  interval = setInterval(update, 100);
  actif=true;
  }

})

$("#stop").click(function (event) {
  clearInterval(interval);
  actif=false;
})

$("#reset").click(function (event) {
  clearInterval(interval);
  chrono = 0;
  actif=false;
  $("#chrono").html(00 + " h: " + 00 + " m:" + 00 + " s: " + 0 + " ms");
})

function update() {
  chrono += 100;

  /*decomposition en ms,s,m et h*/
  var temps = chrono;
  var ms = parseInt(temps % 1000);
  temps = parseInt(temps / 1000);
  var s = parseInt(temps % 60);
  temps = parseInt(temps / 60);
  var m = parseInt(temps % 60);
  temps = parseInt(temps / 60);
  var h = parseInt(temps);

  $("#chrono").html(h + " h: " + m + " m:" + s + " s: " + ms / 100 + " ms");
}