
(async function chargerUser() {
  let users = await fetch('http://localhost:8080/salleafpa/personne');
  let listeUser = await users.json();
  affichageUsers(listeUser);
  console.log(users);
})();

function affichageUsers(listeUser) {
  let tab = ["nom", "prenom", "mail","tel","addresse"];

  let table = document.createElement("table");
  $(table).attr("class", "table table-striped table-bordered");

  let thead = document.createElement("thead");
  let trHead = document.createElement("tr");

  for (i in tab) {
    let th = document.createElement("th");
    $(th).html(tab[i]);
    trHead.append(th);
  }
  thead.append(trHead);
  table.append(thead);

  let tbody = document.createElement("tbody");

  for (i in listeUser) {
    let tr = document.createElement("tr");
    for (j in tab) {
      let td = document.createElement("td");
      $(td).html(listeUser[i][tab[j]]);
      tr.append(td);
    }
    let tdP = document.createElement("td");
    let a = document.createElement("a");
    $(a).attr("href", "listePosts.html?id="+listeUser[i]["id"]);
    $(a).attr("target", "_blank");
    $(a).html("post");
    tdP.append(a);
    tr.append(tdP);
    tbody.append(tr);
  }
  table.append(tbody);

  $("#liste").append(table);
}

