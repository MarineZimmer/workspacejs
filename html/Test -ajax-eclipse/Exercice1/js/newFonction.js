async function chargerListeFonction() {
  let fonction = await fetch('http://localhost:8080/salleafpa/fonction');
  let listeFonction = await fonction.json();
  affichageFonction(listeFonction);
}

function affichageFonction(listeFonction) {
  let tab = ["libelle"];
  $("#listeFonction").children().remove();
  let table = document.createElement("table");
  $(table).attr("class", "table table-striped table-bordered");

  let thead = document.createElement("thead");
  let trHead = document.createElement("tr");

  for (i in tab) {
    let th = document.createElement("th");
    $(th).html("fonction");
    trHead.append(th);
  }
  thead.append(trHead);
  table.append(thead);

  let tbody = document.createElement("tbody");
  for (i in listeFonction) {
    let tr = document.createElement("tr");
    for (j in tab) {
      let td = document.createElement("td");
      $(td).html(listeFonction[i][tab[j]]);
      tr.append(td);
    }

    let tdSuppr = document.createElement("td");
    $(tdSuppr).attr("id", "s" + listeFonction[i]["id"]);
    let supprIcon = document.createElement("i");
    $(supprIcon).attr("class", "far fa-trash-alt");
    $(tdSuppr).click(async function deleteFonction(event) {
      event.preventDefault();
      let id = $(this).attr("id").substring(1);
      let response = await fetch('http://localhost:8080/salleafpa/fonction/delete/' + id, {
        method: 'DELETE'
      })
      chargerListeFonction();
    })
    tdSuppr.append(supprIcon);
    tr.append(tdSuppr);


    tbody.append(tr);
  }
  table.append(tbody);

  $("#listeFonction").append(table);
}

chargerListeFonction();

$("#form").submit(async function (event) {
  event.preventDefault();

  let form = document.getElementById("form");

  //let data = new FormData(form);
  let data = {
    libelle: $("#libelle").val()
  }
  let response = await fetch("http://localhost:8080/salleafpa/fonction/new", {
    method: form.getAttribute('method'),

    body: JSON.stringify(data),

    headers: {
      //"Content-Type": "multipart/form-data" }
      'Accept': 'application/json',
      "Content-type": "application/json; charset=UTF-8"
    }
  })

  let responseData = await response.json();

  chargerListeFonction();
  return false;

})
