
(async function chargerPosts() {

  var urlString = window.location.href;
  var params = urlString.split("?")[1].split("&");

  let userId = params[0].split("=")[1];
  let posts = await fetch("https://jsonplaceholder.typicode.com/posts?userId=" + userId);
  let listePosts = await posts.json();

  $("#userId").attr("value", userId);

  affichagePosts(listePosts);
})();

function affichagePosts(listePosts) {
  let tab = ["title", "body"];

  let table = document.createElement("table");
  $(table).attr("class", "table table-striped table-bordered");

  let thead = document.createElement("thead");
  let trHead = document.createElement("tr");

  for (i in tab) {
    let th = document.createElement("th");
    $(th).html(tab[i]);
    trHead.append(th);
  }
  thead.append(trHead);
  table.append(thead);

  let tbody = document.createElement("tbody");

  for (i in listePosts) {
    let tr = document.createElement("tr");
    let tdT = document.createElement("td");



    let aPost = document.createElement("a");
    $(aPost).attr("href", "");
    $(aPost).attr("id", "titre" + listePosts[i]["id"])
    $(aPost).html(listePosts[i]["title"]);
    tdT.append(aPost);
    tr.append(tdT);

    $(aPost).click(function (event) {
      event.preventDefault();
      let idPost = $(this).attr("id").substring(5);
      afficherComments(idPost);

    });

    let tdB = document.createElement("td");
    $(tdB).html(listePosts[i]["body"].split("\n")[0]);



    let span = document.createElement("span");
    $(span).attr("hidden", "");
    let donnee = listePosts[i]["body"].split("\n");
    donnee.shift();
    $(span).html("\n" + donnee.join("<br>"));
    $(span).attr("id", "span" + listePosts[i]["id"]);
    tdB.append(span);

    let a = document.createElement("a");
    $(a).attr("class", "suite");
    $(a).attr("href", "#");
    $(a).attr("id", "href" + listePosts[i]["id"]);
    $(a).html("   lire la suite");
    tdB.append(a);

    $(a).click(function (event) {
      let id = $(this).attr("id").substring(4);
      let idSpan = "#span" + id;
      $(idSpan).removeAttr("hidden")

      $(this).html("");
      event.preventDefault();
    });


    tr.append(tdB);



    let tdSuppr = document.createElement("td");
    let suppr = document.createElement("href");
    $(suppr).attr("id", "s" + listePosts[i]["id"]);
    let supprIcon = document.createElement("i");
    $(supprIcon).attr("class", "far fa-trash-alt");
    $(suppr).click(async function deletePost(event) {
      event.preventDefault();
      let id = $(this).attr("id").substring(1);
      let response = await fetch('https://jsonplaceholder.typicode.com/posts/' + id, {
        method: 'DELETE'
      })
      alert("post supprime ")

    })
    suppr.append(supprIcon);
    tdSuppr.append(suppr);
    tr.append(tdSuppr);

    let tdModif = document.createElement("td");
    let modif = document.createElement("a");
    $(modif).attr("href", "modifPost.html?id=" + listePosts[i]["id"]);
    let modifIcon = document.createElement("i");
    $(modifIcon).attr("class", "fas fa-pencil-alt");

    modif.append(modifIcon);
    tdModif.append(modif);
    tr.append(tdModif);

    tbody.append(tr);
  }
  table.append(tbody);

  $("#liste").append(table);
}




async function afficherComments(id) {
  let comments = await fetch("https://jsonplaceholder.typicode.com/posts/" + id + "/comments");
  let listeComments = await comments.json();
  $("#comment").children().remove();
  $("#comment").html("Commentaires : ");



  let tab = ["email", "name", "body"];
  let table = document.createElement("table");
  $(table).attr("class", "table table-striped table-bordered");

  let thead = document.createElement("thead");
  let trHead = document.createElement("tr");

  for (i in tab) {
    let th = document.createElement("th");
    $(th).html(tab[i]);
    trHead.append(th);
  }
  thead.append(trHead);
  table.append(thead);

  let tbody = document.createElement("tbody");
  for (i in listeComments) {
    let tr = document.createElement("tr");
    for (j in tab) {
      let td = document.createElement("td");
      $(td).html(listeComments[i][tab[j]]);
      tr.append(td);
    }


    tbody.append(tr);
  }
  table.append(tbody);



  $("#comment").append(table);

}





