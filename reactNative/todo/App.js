import React, { useState, useEffect } from 'react';
import { TextInput, FlatList, StyleSheet, Alert, View,Dimensions,Keyboard  } from 'react-native'
import { CheckBox, Text, Button, Input, Item } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';


export default function App() {
  const [todo, setTodo] = useState("");
  const [todoList, setTodoList] = useState([]);
  const [modif, setModif] = useState(false);
  const [isLoading, setIsLoading] = useState(true);


  const getTodos = async () => {
     await  fetch( "http://10.115.59.27:8080/salleafpa/todos")
     .then(response => {
        return response.json();
    })
     .then(data=>setTodoList(data))
     .catch(error => {
         console.log(error);
     }
     ); 
  }



  async function addTodo() {
    //setTodoList([...todoList, { id: id, todo: todo.text, checked: false }]);

   
     var data = {
       todo: todo.text,
       checked: false
     }
     await fetch("http://10.115.59.27:8080/salleafpa/todos", {
       method: "POST",
       body: JSON.stringify( data),
       headers: {
         "Content-type": "application/json; charset=UTF-8"
       }
     })
       .then(response => response.json())
       .catch(error => console.log(error));
   
     Keyboard.dismiss();
     setTodo("");
     setModif(!modif);
  }

  function removeTodo(id) {

   
       fetch("http://10.115.59.27:8080/salleafpa/todos/" + id, {
          method: "DELETE"
      })
          .catch(error => { console.log(error); });
     
    
  

   /* const tabTodo = todoList;
    const todoRemove = tabTodo.find(e => e.id == id)
    var pos = tabTodo.indexOf(todoRemove);
    tabTodo.splice(pos, 1);
    setTodoList(tabTodo);*/
    setModif(!modif);
  }

   function checkedTodo(id) {

    const todoChecked = todoList.find(e => e.id == id)
    var data = {
      id:todoChecked.id,
      todo: todoChecked.todo,
      checked: !todoChecked.checked
    }
    let responseData =  fetch("http://10.115.59.27:8080/salleafpa/todos", {
      method: "PUT",
      body: JSON.stringify( data),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then(response => response.json())
      .catch(error => console.log(error));

   /* const tabTodo = todoList;
    const todoChecked = tabTodo.find(e => e.id == id)
    var pos = tabTodo.indexOf(todoChecked);
    todoChecked.checked = !todoChecked.checked
    tabTodo.splice(pos, 1, todoChecked);
    setTodoList(tabTodo);
    todoList.sort((todo1,todo2) => {
      if(todo1.checked===todo2.checked){
        return todo1.id-todo2.id;
      }else if(todo1.checked){
        return 1;
      }else{
        return -1;
      }
    })*/
    setModif(!modif);
  }


  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
       await getTodos();
      setIsLoading(false);
    }
    fetchData()
  }, [modif])
  

  function Item2({ todo, checked, id }) {
    todoList.sort((todo1,todo2) => {
      if(todo1.checked===todo2.checked){
        return todo1.id-todo2.id;
      }else if(todo1.checked){
        return 1;
      }else{
        return -1;
      }
    })
    return (
      <View style={styles.item}>
        <CheckBox  style={styles.checkedBox}
          checked={checked}
          onPress={checkedTodo.bind(todo, id)} />
        <Text style={[ styles.texte, {textDecorationLine: (checked ? 'line-through' : 'none')}]}>{todo}</Text>
        <Icon style={styles.icon}
          name='trash'
          size={25}
          onPress={removeTodo.bind(todo, id)} />
      </View>
    );
  }


  return ( <View style={styles.container}>
    <View style={styles.texteTitre}>
      <Text style={styles.texteTitre} >Todo List</Text></View>
    <View style={styles.rowAddTodo}>
      <Item inlineLabel style={styles.inputTodo}>
        <Input
          style={styles.inputTodo}
          placeholder="add Todos"
          onChangeText={(text) => setTodo({ text })}
          value={todo}
          maxLength = {19}
        />

      </Item >
      <Button bordered primary style={styles.buttonAdd}
        onPress={addTodo}
      ><Text style={styles.texteButtonAdd}>Add</Text></Button>
    </View>

    <FlatList
      data={todoList}

      renderItem={({ item }) => <Item2 todo={item.todo} checked={item.checked} id={item.id} />}
      keyExtractor={item => item.id}
    />
  </View>
)
}

const styles = StyleSheet.create({
  container: {
    flex: -1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginTop: 23,

  },
  inputTodo: {
    padding: 10,
    margin: 10,
    height: 40,
    width: 250,
    borderColor: 'orange',
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  buttonAdd: {
    margin: 10,
    backgroundColor: 'orange',
    borderRadius: 35,
    borderColor : 'orange'

  },
  rowAddTodo: {
    flex: -1,
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  item: {
    flexDirection: 'row',
    marginLeft: 10,
    paddingTop: 10,
    width: Dimensions.get('screen').width-40,
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor : 'orange',
    borderStyle:'dotted',

  },
  texte: {
    paddingLeft: 20,
    color: 'grey',
    fontSize: 25,
  },
  texteButtonAdd: {
    alignItems: 'stretch',
    color: 'white',
  },
  icon: {
    paddingLeft: 20,
    justifyContent: 'flex-end',
  },
  checkedBox: {
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    marginTop: 10,
    marginLeft: 10,
    backgroundColor: 'orange',
    borderColor: 'orange',

  },
  texteTitre: {
    flex: -1,
    alignItems: 'center',
    paddingLeft: 20,
    width: 400,
    height: 50,
    color: 'white',
    backgroundColor: 'orange',
    fontSize: 25,
  },

});
